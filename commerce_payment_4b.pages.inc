<?php

/**
 * Order details to reply to 4b payment.
 */
function commerce_payment_4b_checkout_details($payment_method = NULL) {
  $response = array();

  $response['order_id'] = intval($_REQUEST['order']);
  $response['store'] = check_plain($_REQUEST['store']);

  $config = _commerce_payment_4b_get_rule_config('commerce_payment_4b', 'commerce_payment_commerce_payment_4b');

  if (!is_numeric($response['order_id'])) {
    watchdog('commerce_payment_4b', t('Invalid Order ID'), array(), WATCHDOG_ERROR);
  } 
  else if ($response['store'] != $config['commerce_4b_store']) {
    watchdog('commerce_payment_4b', t('Invalid Store ID'), array(), WATCHDOG_ERROR);
  } 
  else {
    $order = commerce_order_load($response['order_id']);
    $price = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'] * 100;
    $price = round($price);

    $currency = $config['commerce_4b_currency'];
    $items = 1;     // Only inform to the platform one item and total price.
    $shop = $config['commerce_4b_store'];

    print 'M' . $currency . $price . "\n";
    print $items . "\n";
    print '1' . "\n";    // Reference number.
    print $shop . "\n";  // Description.
    print '1' . "\n";    // Quantity.
    print $price . "\n"; // Price.
  }
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_payment_4b_redirect_page() {
  $response = array();

  $response['order_id'] = intval($_REQUEST['pszPurchorderNum']);
  $response['status'] = intval($_REQUEST['result']);
  $response['store'] = check_plain($_REQUEST['store']);
  $response['pszTxnDate'] = check_Plain($_REQUEST['pszTxnDate']);
  $response['tipotrans'] = check_Plain($_REQUEST['tipotrans']);
  $response['pszApprovalCode'] = check_Plain($_REQUEST['pszApprovalCode']);
  $response['pszTxnID'] = check_Plain($_REQUEST['pszTxnID']);
  $response['MAC'] = check_Plain($_REQUEST['MAC']);

  $origin  = ip_address();
  $config = _commerce_payment_4b_get_rule_config('commerce_payment_4b', 'commerce_payment_commerce_payment_4b');
  if (!$config['commerce_4b_use_testing']) {
    if ($origin != $config['commerce_4b_production_ip']) {
      watchdog('commerce_payment_4b', t('Error: Invalid origin IP address.'), array(), WATCHDOG_ERROR); 
      drupal_goto("cart");
    }
  }
  if (!$response['status'] || $response['status'] != 0) {
    watchdog('commerce_payment_4b', t('Error: Payment process fails.'), array(), WATCHDOG_ERROR);
    drupal_goto("cart");
  } 
  else {
    $order = commerce_order_load($response['order_id']);
    commerce_payment_4b_transaction($payment_method, $order, $response, COMMERCE_PAYMENT_STATUS_SUCCESS);
  }

  drupal_goto("checkout/" . $order->order_id . "/complete");
}


/**
 * Get all settings rules of payment method
 * 
 * @param type $method_id
 * @param type $rule_name
 * @return type 
 */
function _commerce_payment_4b_get_rule_config($method_id, $rule_name) {
  $rule = rules_config_load($rule_name);

  $settings = array();

  foreach ($rule->actions() as $action) {
    if ($action->getElementName() == 'commerce_payment_enable_' . $method_id) {
      if (is_array($action->settings['payment_method']) && !empty($action->settings['payment_method']['settings'])) {
        $settings = $action->settings['payment_method']['settings'];
      }
    }
  }

  return $settings;
}